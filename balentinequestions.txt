Do all teachers currently log attendance online at the end of the day?
If they do, what is their current method of doing so? How does the school collect this information?

If so, could the district assign teachers to log attendance online at designated times?
Do teachers report attendance after each period or at the end of the day?

In the current system, is attendance already included in report cards?
How many absences are needed for an immediate action by the school?

If so, is it automatically pulled in from current attendance reports or is it manually entered?
Is the attendance data on tangible paper or online?

For the students, if they miss one day of classes (5-7 periods), is that counted as 1 absence or
is it an absence per class?
Do you also account for tardiness as well as absence?

If it is per class, is that right to send a warning just for one day out?
What is the delay between collection of attendance information and notice to the parents if MIA?